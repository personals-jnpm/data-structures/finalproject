package repositories;

import models.Country;

import java.util.TreeMap;

public class CountryRepository {

    private final TreeMap<Integer, Country> countryMap;
    private int position;

    public CountryRepository() {
        countryMap = new TreeMap<>();
        position = 9;
        initCountryMap();
    }

    public TreeMap<Integer, Country> getCountryMap() {
        return countryMap;
    }

    public void addCountryToTree(Country country){
        country.setCountryId(position);
        countryMap.put(position++, country);
    }

    public Country findCountryInMap(Integer countryKey){
        return countryMap.get(countryKey);
    }

    public void initCountryMap(){
        countryMap.put(0, new Country(0, "Colombia"));
        countryMap.put(1, new Country(1, "Panamá"));
        countryMap.put(2, new Country(2, "Estados Unidos"));
        countryMap.put(3, new Country(3, "Brasil"));
        countryMap.put(4, new Country(4, "Perú"));
        countryMap.put(5, new Country(5, "Egipto"));
        countryMap.put(6, new Country(6, "Alemania"));
        countryMap.put(7, new Country(7, "China"));
        countryMap.put(8, new Country(8, "Chile"));
    }
}
