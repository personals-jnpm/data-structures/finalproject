package repositories;

import models.*;

import java.util.ArrayList;
import java.util.List;

public class RouteSystemRepository {

    private final RouteSystem routeSystem;
    private final CityRepository cityRepository = new CityRepository();
    private int positionRoute;

    public RouteSystemRepository() {
        routeSystem = new RouteSystem();
        positionRoute = 8;
        initNodes2();
    }

    public RouteSystem getRouteSystem() {
        return routeSystem;
    }

    public void addNode(int cityId) {
        try {
            City city = cityRepository.findCityById(cityId);
            routeSystem.addNode(city);
        } catch (Exception e) {
            System.out.println(" ¡El código de la ciudad no es válido (ಠ︵ಠ)!");
        }
    }

    public void addRouteToNode(int cityId, Route route) {
        route.setRouteId(positionRoute++);
        routeSystem.addRouteToNode(cityId, route);
    }

    public List<Node> getNodes(){
        return routeSystem.getNodes();
    }

    public Node searchNodeByCityId(int cityId){
        return routeSystem.searchNodeByCityId(cityId);
    }

    public Route searchRouteById(int routeId){
        return routeSystem.searchRouteById(routeId);
    }

    public List<City> getAllCities(){
        return routeSystem.getAllCities();
    }

    public List<Route> findBestRoute(Node origin, Node destination) {
        return routeSystem.findBestRoute(origin, destination);
    }

    private void initNodes() {
        for (int i = 0; i < 21; i++) {
            addNode(i);
        }
        int routeId = 0;
        for (int i = 0; i < 21; i++) {
            List<Integer> nodes = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                int destinationIndex = getDestinationIndex(i, nodes);
                addRouteToNode(i, new Route(routeId++, routeSystem.searchNodeByCityId(i),
                        routeSystem.searchNodeByCityId(destinationIndex),
                        Math.random() * 72, (int) (Math.random() * (901) + 100)));
                nodes.add(destinationIndex);
            }
        }
    }

    private int getDestinationIndex(int current, List<Integer> nodes){
        do {
            int destinationIndex = (int) (Math.random() * 21);
            if (destinationIndex != current && !nodes.contains(destinationIndex)){
                return destinationIndex;
            }
        } while (true);
    }

    private void initNodes2(){
        Node bogota = new Node(cityRepository.getCities()[0]);
        Node cartagena = new Node(cityRepository.getCities()[1]);
        Node cali = new Node(cityRepository.getCities()[2]);
        Node tunja = new Node(cityRepository.getCities()[3]);
        Node bucaramanga = new Node(cityRepository.getCities()[4]);
        Node panama = new Node(cityRepository.getCities()[5]);

        bogota.addRoute(new Route(0, bogota, cartagena, 5, 280));
        bogota.addRoute(new Route(1, bogota, bucaramanga, 4, 304));
        cartagena.addRoute(new Route(2, cartagena, cali, 15, 52));
        cali.addRoute(new Route(3, cali, tunja, 2, 296));
        bucaramanga.addRoute(new Route(4, bucaramanga, panama, 8, 283));
        cali.addRoute(new Route(5, cali, bucaramanga, 3, 237));
        tunja.addRoute(new Route(6, tunja, bucaramanga, 6, 102));
        tunja.addRoute(new Route(7, tunja, panama, 1, 383));

        List<Node> nodesList = new ArrayList<>();

        nodesList.add(bogota);
        nodesList.add(cartagena);
        nodesList.add(cali);
        nodesList.add(tunja);
        nodesList.add(bucaramanga);
        nodesList.add(panama);

        routeSystem.setNodes(nodesList);
    }
}
