package repositories;

import models.City;
import models.Country;
import models.Weather;

import java.util.TreeMap;

public class CityRepository {

    private static final int CITIES_NUMBER = 25;
    private int position;
    private final City[] cities;
    private final CountryRepository countryRepository = new CountryRepository();

    public CityRepository() {
        position = 21;
        cities = new City[CITIES_NUMBER];
        initCities();
    }

    public City[] getCities() {
        return cities;
    }

    public TreeMap<Integer, Country> getCountries(){
        return countryRepository.getCountryMap();
    }

    public void addCity(City city, int countryId){
        city.setCityId(position);
        city.setCountry(countryRepository.findCountryInMap(countryId));
        cities[position++] = city;
    }

    public City findCityById(int cityId){
        for (City city: cities) {
            if (city.getCityId() == cityId){
                return city;
            }
        }
        return null;
    }

    private void initCities(){
        cities[0] = new City(0, "Bogotá", countryRepository.findCountryInMap(0), Weather.Frío, 2630);
        cities[1] = new City(1, "Cartagena", countryRepository.findCountryInMap(0), Weather.Cálido, 10);
        cities[2] = new City(2, "Cali", countryRepository.findCountryInMap(0), Weather.Templado, 1118);
        cities[3] = new City(3, "Tunja", countryRepository.findCountryInMap(0), Weather.Frío, 2810);
        cities[4] = new City(4, "Bucaramanga", countryRepository.findCountryInMap(0), Weather.Templado, 959);
        cities[5] = new City(5, "Ciudad de Panamá", countryRepository.findCountryInMap(1), Weather.Cálido, 500);
        cities[6] = new City(6, "Miami", countryRepository.findCountryInMap(2), Weather.Cálido, 2);
        cities[7] = new City(7, "New York", countryRepository.findCountryInMap(2), Weather.Helado, 10);
        cities[8] = new City(8, "Rio de janeiro", countryRepository.findCountryInMap(3), Weather.Cálido, 3);
        cities[9] = new City(9, "Brasilia", countryRepository.findCountryInMap(3), Weather.Templado, 1172);
        cities[10] = new City(10, "Lima", countryRepository.findCountryInMap(4), Weather.Templado, 161);
        cities[11] = new City(11, "Arequipa", countryRepository.findCountryInMap(4), Weather.Frío, 3);
        cities[12] = new City(12, "Los ángeles", countryRepository.findCountryInMap(2), Weather.Cálido, 93);
        cities[13] = new City(13, "Berlín", countryRepository.findCountryInMap(6), Weather.Helado, 34);
        cities[14] = new City(14, "Frankfurt", countryRepository.findCountryInMap(6), Weather.Helado, 112);
        cities[15] = new City(15, "Pekín", countryRepository.findCountryInMap(7), Weather.Frío, 44);
        cities[16] = new City(16, "Shanghái", countryRepository.findCountryInMap(7), Weather.Frío, 4);
        cities[17] = new City(17, "Xi'an", countryRepository.findCountryInMap(7), Weather.Templado, 405);
        cities[18] = new City(18, "Santiago", countryRepository.findCountryInMap(8), Weather.Templado, 570);
        cities[19] = new City(19, "El cairo", countryRepository.findCountryInMap(5), Weather.Cálido, 23);
        cities[20] = new City(20, "Cusco", countryRepository.findCountryInMap(4), Weather.Frío, 3399);
    }

}
