package repositories;

import models.Client;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ClientRepository {

    private static final int CLIENTS_NUMBER = 25;
    private int position;
    private Client[] clients;

    private Queue<Client> clientQueue;

    public ClientRepository() {
        position = 10;
        clients = new Client[CLIENTS_NUMBER];
        clientQueue = new LinkedList<>();
        initClients();
        generateRandomOrder();
    }

    public Client[] getClients() {
        return clients;
    }

    public Queue<Client> getClientQueue() {
        return clientQueue;
    }

    public void addClient(Client client){
        client.setClientId(position);
        clients[position++] = client;
    }

    public void updateClient(Client client){
        clients[client.getClientId()] = client;
    }

    public void serveClient() {
        clientQueue.poll();
    }

    public Client getCurrentClient() {
        return clientQueue.peek();
    }


    public void printClientQueue() {
        System.out.println("Clientes en cola [");
        for (Client cliente : clientQueue) {
            System.out.println("\t" + cliente.toString());
        }
        System.out.print("]\n");
    }

    public void printClients() {
        System.out.println("Clientes de la aerolínea [");
        for (int i = 0; i < position; i++) {
            System.out.println("\t" + clients[i].toString());
        }
        System.out.print("]\n");
    }


    private void generateRandomOrder() {
        List<Integer> listaTemporal = new ArrayList<>();
        for (int i = 0; i < position; i++) {
            int idCliente = (int) (Math.random() * (9));
            if (i == 0) {
                listaTemporal.add(idCliente);
                clientQueue.add(findClientById(idCliente));
            } else if (!listaTemporal.contains(idCliente)) {
                listaTemporal.add(idCliente);
                clientQueue.add(findClientById(idCliente));
            }
        }
    }

    public Client findClientById(int clientId){
        for (Client client: clients) {
            if (client.getClientId() == clientId){
                return client;
            }
        }
        return null;
    }

    private void initClients(){
        clients[0] = new Client(0,"1254123654", "Carlos", 0);
        clients[1] = new Client(1,"5423687", "María", 0);
        clients[2] = new Client(2,"45678214", "Diana", 0);
        clients[3] = new Client(3,"1021458793", "Oscar", 0);
        clients[4] = new Client(4,"4563217893", "Mariana", 0);
        clients[5] = new Client(5,"65478963", "Catalina", 0);
        clients[6] = new Client(6,"1324563218", "Fernando", 0);
        clients[7] = new Client(7,"4521369", "Andrés", 0);
        clients[8] = new Client(8,"3256348521", "Nicolás", 0);
        clients[9] = new Client(9,"7894521362", "Laura", 0);
    }
}
