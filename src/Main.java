import controllers.CityController;
import controllers.ClientController;
import controllers.RouteSystemController;
import models.AirlineFlight;
import models.City;
import models.Country;
import models.Node;
import utiliy.Keyboard;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

public class Main {

    public static Keyboard keyboard = new Keyboard();

    public static CityController cityController = new CityController();
    public static ClientController clientController = new ClientController();
    public static RouteSystemController routeSystemController = new RouteSystemController();

    public static void main(String[] args) {
        Main main = new Main();
        main.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> main.registerClient();
                case 2 -> main.serveClient();
                case 3 -> main.registerCity();
                case 4 -> main.addNode();
                case 5 -> main.registerNewRoutes();
                case 6 -> main.showClients();
                case 7 -> main.showClientQueue();
                case 8 -> main.showCities();
                case 9 -> main.showNodes();
                case 10 -> main.getFlightByClientId();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔═══════════════════════════════════════════════════════════╗");
        System.out.println("╠-------------------Aerolínea Saint Tomas-------------------╣");
        System.out.println("║═══════════════════════════════════════════════════════════║");
        System.out.println("║   1. Registrar cliente                                    ║");
        System.out.println("║   2. Atender cliente (Asignar ticked)                     ║");
        System.out.println("║   3. Registrar ciudad                                     ║");
        System.out.println("║   4. Agregar ciudad al sistema de rutas (grafo)           ║");
        System.out.println("║   5. Agregar nueva ruta                                   ║");
        System.out.println("║═══════════════════════════════════════════════════════════║");
        System.out.println("║   6. Mostrar clientes de la aerolínea                     ║");
        System.out.println("║   7. Mostrar cola de clientes                             ║");
        System.out.println("║   8. Mostrar ciudades                                     ║");
        System.out.println("║   9. Mostrar sistema de rutas (grafo)                     ║");
        System.out.println("║   10. Mostrar lista de vuelos por cliente                 ║");
        System.out.println("║═══════════════════════════════════════════════════════════║");
        System.out.println("║   0. Salir                                                ║");
        System.out.println("╚═══════════════════════════════════════════════════════════╝");
    }

    public void registerClient() {
        String IDNumber = keyboard.readLineDefault("  Ingrese la cédula del cliente: ");
        String name = keyboard.readLineDefault("  Ingrese el nombre del cliente: ");
        clientController.addClient(IDNumber, name);
    }

    public void serveClient() {
        printCitiesInRouteSystem();
        System.out.println(" Cliente actual " + clientController.getCurrentClient().getClientName() + ": ");
        int origen = keyboard.readValidPositiveInteger("  Ingrese el código de la ciudad de origen: ");
        int destination = keyboard.readValidPositiveInteger("  Ingrese el código de la ciudad de destino: ");
        double price = keyboard.readValidPositiveDouble("  Ingrese el precio del vuelo: ");
        int startHour = keyboard.readValidHour("  Ingrese la hora de inicio del vuelo (0-24h): ");
        int endHour = keyboard.readValidHour("  Ingrese la hora de llagada del vuelo (0-24h): ");
        Date date = new Date();
        clientController.addFlight(origen, destination, price, startHour, endHour, date, true);
    }

    public void registerCity() {
        String name = keyboard.readLineDefault("  Ingrese el nombre de la ciudad: ");
        printCountries();
        int countryId = keyboard.readValidPositiveInteger("  Ingrese el código del país: ");
        String weather = keyboard.readLineDefault("  Ingrese el clima (Cálido, Templado, Frío, Helado): ");
        double height = keyboard.readValidPositiveDouble("  Ingrese la altura de la ciudad: ");
        cityController.addCity(name, countryId, weather, height);
    }

    private void printCountries() {
        try {
            TreeMap<Integer, Country> countries = cityController.getCountries();
            System.out.println("\nPaíses [");
            for (Integer countryKey : countries.keySet()) {
                System.out.println("\t[" + countryKey + "] " + countries.get(countryKey));
            }
        } catch (Exception ignored) {
        }
        System.out.print("]\n");
    }

    public void addNode() {
        showCities();
        int cityId = keyboard.readValidPositiveInteger("\n  Ingrese el código de la ciudad: ");
        routeSystemController.addNode(cityId);
    }

    public void registerNewRoutes() {
        printCitiesInRouteSystem();
        int origen = keyboard.readValidPositiveInteger("  Ingrese el código de la ciudad de origen: ");
        int destination = keyboard.readValidPositiveInteger("  Ingrese el código de la ciudad de destino: ");
        double travelTime = keyboard.readValidPositiveDouble("  Ingrese el tiempo de vuelo en horas: ");
        double miles = keyboard.readValidPositiveDouble("  Ingrese las millas de distancia entre las dos ciudades: ");
        routeSystemController.addRouteToNode(origen, destination, travelTime, miles);
    }

    private void printCitiesInRouteSystem() {
        System.out.println("\nCiudades agregadas al sistema de rutas [");
        for (City city : routeSystemController.getAllCities()) {
            System.out.println("\t[" + city.getCityId() + "] " + city);
        }
        System.out.print("]\n");
    }

    public void showClients() {
        clientController.printClients();
    }

    public void showClientQueue() {
        clientController.printClientQueue();
    }

    public void showCities() {
        try {
            City[] cities = cityController.getCities();
            System.out.println("\nCiudades [");
            for (City city : cities) {
                System.out.println("\t[" + city.getCityId() + "] " + city);
            }
        } catch (Exception ignored) {
        }
        System.out.print("]\n");
    }

    public void showNodes() {
        System.out.println("\nSistema de rutas [");
        for (Node node : routeSystemController.getNodes()) {
            System.out.println("\t[" + node.getCity().getCityId() + "] " + node);
        }
        System.out.print("]\n");
    }

    public void getFlightByClientId() {
        showClients();
        try {
            int clientId = keyboard.readValidPositiveInteger("  Ingrese el código del cliente: ");
            System.out.println("\nVuelos del cliente " + clientController.findClientById(clientId).getClientName() + " [");

            List<AirlineFlight> flights = clientController.getFlightsByClient(clientId);
            flights.sort(Comparator.comparing(AirlineFlight::getDate));

            for (AirlineFlight flight : flights) {
                System.out.println("\t" + flight);
            }
        } catch (Exception ignored) {
        }
        System.out.print("]\n");
    }

}
