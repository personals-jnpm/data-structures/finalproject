package controllers;

import models.City;
import models.Country;
import models.Weather;
import repositories.CityRepository;

import java.util.TreeMap;

public class CityController {

    public CityRepository cityRepository;

    public CityController() {
        cityRepository = new CityRepository();
    }

    public City[] getCities() {
        return cityRepository.getCities();
    }

    public void addCity(String cityName, int countryId, String weather, double height){
        City city = new City(cityName, null, getTypeWeather(weather), height);
        cityRepository.addCity(city, countryId);
    }

    public TreeMap<Integer, Country> getCountries(){
        return cityRepository.getCountries();
    }

    public City findCityById(int cityId){
        return cityRepository.findCityById(cityId);
    }

    private Weather getTypeWeather(String option)  {
        switch (option) {
            case "Cálido", "cálido" -> {
                return Weather.Cálido;
            }
            case "Templado", "templado" -> {
                return Weather.Templado;
            }
            case "Frío", "frío" -> {
                return Weather.Frío;
            }
            case "Helado", "helado" -> {
                return Weather.Helado;
            }
            default -> {
                return Weather.SinDefinir;
            }
        }
    }

}
