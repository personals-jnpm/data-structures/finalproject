package controllers;

import models.City;
import models.Node;
import models.Route;
import repositories.RouteSystemRepository;

import java.util.List;

public class RouteSystemController {

    public RouteSystemRepository routeSystemRepository;

    public RouteSystemController() {
        routeSystemRepository = new RouteSystemRepository();
    }

    public void addNode(int cityId) {
        routeSystemRepository.addNode(cityId);
    }

    public void addRouteToNode(int origin, int destination, double travelTime, double miles){
        Route route = new Route(searchNodeByCityId(origin), searchNodeByCityId(destination), travelTime, miles);
        routeSystemRepository.addRouteToNode(origin, route);
    }

    public Node searchNodeByCityId(int cityId){
        return routeSystemRepository.searchNodeByCityId(cityId);
    }

    public Route searchRouteById(int routeId){
        return routeSystemRepository.searchRouteById(routeId);
    }

    public List<Route> findBestRoute(int origin, int destination) {
        return routeSystemRepository.findBestRoute(searchNodeByCityId(origin), searchNodeByCityId(destination));
    }

    public List<Node> getNodes(){
        return routeSystemRepository.getNodes();
    }

    public List<City> getAllCities(){
        return routeSystemRepository.getAllCities();
    }
}
