package controllers;

import models.*;
import repositories.ClientRepository;
import repositories.RouteSystemRepository;

import java.util.Date;
import java.util.List;

public class ClientController {

    private final ClientRepository clientRepository;
    private final RouteSystemRepository routeSystemRepository;

    private static final double INITIAL_MILES = 0;

    private Client currentClient;

    public ClientController() {
        clientRepository = new ClientRepository();
        routeSystemRepository = new RouteSystemRepository();
        addCurrentClient();
    }

    public Client getCurrentClient() {
        return currentClient;
    }

    public void addCurrentClient() {
        currentClient = clientRepository.getCurrentClient();
    }

    public void serveClient() {
        clientRepository.serveClient();
        addCurrentClient();
    }

    public void addClient(String IDNumber, String clientName) {
        Client client = new Client(IDNumber, clientName, INITIAL_MILES);
        clientRepository.addClient(client);
    }

    public void addFlight(int origin, int destination, double price, int startHour, int endHour, Date date, boolean state) {
        Client client = currentClient;
        Schedule schedule = new Schedule(startHour, endHour);
        try {
            client.addFlight(new AirlineFlight(routeSystemRepository.searchNodeByCityId(origin),
                    routeSystemRepository.searchNodeByCityId(destination), price, schedule, date, state));
            clientRepository.updateClient(client);
            printScales();
            serveClient();
        } catch (Exception e) {
            System.out.println("\n  ¡Ruta no disponible (ಠ︵ಠ)!");
        }
    }

    public void printClientQueue() {
        clientRepository.printClientQueue();
    }

    public void printClients() {
        clientRepository.printClients();
    }

    public List<AirlineFlight> getFlightsByClient(int clientId) {
        return clientRepository.findClientById(clientId).getFlights();
    }

    public Client findClientById(int clientId) {
        return clientRepository.findClientById(clientId);
    }

    private void printScales() {
        System.out.println("\nEscalas de vuelo necesarias [");
        for (Route route : currentClient.getFlights().getLast().getScales()) {
            System.out.println("\t" + route);
        }
        System.out.print("]\n");
    }
}
