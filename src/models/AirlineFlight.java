package models;

import repositories.RouteSystemRepository;

import java.util.Date;
import java.util.List;
import java.util.Stack;

public class AirlineFlight {

    private int flightId;
    private final Stack<Route> scales;
    private Node origin;
    private Node destination;
    private double price;
    private Schedule schedule;
    private Date date;
    private boolean state;

    private final RouteSystemRepository routeSystemRepository;


    public AirlineFlight() {
        scales = new Stack<>();
        routeSystemRepository = new RouteSystemRepository();
    }

    public AirlineFlight(Node origin, Node destination, double price, Schedule schedule, Date date, boolean state) {
        this.origin = origin;
        this.destination = destination;
        this.price = price;
        this.schedule = schedule;
        this.date = date;
        this.state = state;
        scales = new Stack<>();
        routeSystemRepository = new RouteSystemRepository();
        getBestRoute();
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public Stack<Route> getScales() {
        return scales;
    }

    public Node getOrigin() {
        return origin;
    }

    public void setOrigin(Node origin) {
        this.origin = origin;
    }

    public Node getDestination() {
        return destination;
    }

    public void setDestination(Node destination) {
        this.destination = destination;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    private void getBestRoute() {
        List<Route> bestRoute = routeSystemRepository.findBestRoute(origin, destination);
        if (bestRoute != null) convertListToStack(bestRoute);
    }

    private void convertListToStack(List<Route> routes){
        for (Route route: routes) {
            scales.push(route);
        }
    }

    @Override
    public String toString() {
        return "{" +
                "flightId=" + flightId +
                ", scales=" + scales +
                ", origin=" + origin.getCity().getCityName() +
                ", destination=" + destination.getCity().getCityName() +
                ", price=" + price +
                ", schedule=" + schedule +
                ", date=" + date +
                ", state=" + state +
                '}';
    }
}
