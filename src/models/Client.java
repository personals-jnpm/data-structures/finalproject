package models;

import java.util.LinkedList;

public class Client {

    private int clientId;
    private String IDNumber;
    private String clientName;
    private double accumulatedMiles;
    private final LinkedList<AirlineFlight> flights;

    private int positionFlight;

    public Client() {
        flights = new LinkedList<>();
        positionFlight = 0;
    }

    public Client(String IDNumber, String clientName, double accumulatedMiles) {
        this.IDNumber = IDNumber;
        this.clientName = clientName;
        this.accumulatedMiles = accumulatedMiles;
        flights = new LinkedList<>();
        positionFlight = 0;
    }

    public Client(int clientId, String IDNumber, String clientName, double accumulatedMiles) {
        this.clientId = clientId;
        this.IDNumber = IDNumber;
        this.clientName = clientName;
        this.accumulatedMiles = accumulatedMiles;
        flights = new LinkedList<>();
        positionFlight = 0;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getIDNumber() {
        return IDNumber;
    }

    public void setIDNumber(String IDNumber) {
        this.IDNumber = IDNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public double getAccumulatedMiles() {
        return accumulatedMiles;
    }

    public void setAccumulatedMiles(double accumulatedMiles) {
        this.accumulatedMiles = accumulatedMiles;
    }

    public LinkedList<AirlineFlight> getFlights() {
        return flights;
    }

    public void addFlight(AirlineFlight flight){
        sumMiles(flight);
        flight.setFlightId(positionFlight++);
        flights.add(flight);
    }

    private void sumMiles(AirlineFlight flight){
        if (flight.getScales() != null){
            for (Route route: flight.getScales()) {
                accumulatedMiles += route.getMiles();
            }
        }
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", IDNumber='" + IDNumber + '\'' +
                ", clientName='" + clientName + '\'' +
                ", accumulatedMiles=" + accumulatedMiles +
                ", flights=" + flights +
                '}';
    }
}
