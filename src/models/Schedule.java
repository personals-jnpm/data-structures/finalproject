package models;

public class Schedule {

    private int startHour;
    private int endHour;

    public Schedule() {
    }

    public Schedule(int startHour, int endHour) {
        this.startHour = startHour;
        this.endHour = endHour;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    @Override
    public String toString() {
        return "{" +
                " startHour=" + startHour +
                ", endHour=" + endHour +
                '}';
    }
}
