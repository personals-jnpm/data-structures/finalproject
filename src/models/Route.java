package models;

public class Route {

    private int routeId;
    private Node origin;
    private Node destination;
    private double travelTime;
    private double miles;

    public Route() {
    }

    public Route(int routeId, Node origin, Node destination, double travelTime, double miles) {
        this.routeId = routeId;
        this.origin = origin;
        this.destination = destination;
        this.travelTime = travelTime;
        this.miles = miles;
    }

    public Route(Node origin, Node destination, double travelTime, double miles) {
        this.origin = origin;
        this.destination = destination;
        this.travelTime = travelTime;
        this.miles = miles;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public Node getOrigin() {
        return origin;
    }

    public void setOrigin(Node origin) {
        this.origin = origin;
    }

    public Node getDestination() {
        return destination;
    }

    public void setDestination(Node destination) {
        this.destination = destination;
    }

    public double getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(double travelTime) {
        this.travelTime = travelTime;
    }

    public double getMiles() {
        return miles;
    }

    public void setMiles(double miles) {
        this.miles = miles;
    }

    @Override
    public String toString() {
        return  "{ origin: " + origin.getCity().getCityName() +
                ", destination: " + destination.getCity().getCityName() +
                ", travelTime: " + travelTime +
                ", miles: " + miles  + " }";
    }
}
