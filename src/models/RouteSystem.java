package models;

import java.util.*;

public class RouteSystem {

    private List<Node> nodes;

    private Map<Node, Integer> separator;
    private Map<Node, List<Integer>> separatorRoutes;

    private Map<Integer, List<Integer>> sumMiles;

    private int sum;
    private List<Integer> routesSaved;

    public RouteSystem() {
        nodes = new ArrayList<>();
        initLocalVariables();
    }

    private void initLocalVariables() {
        sum = 0;
        separator = new HashMap<>();
        separatorRoutes = new HashMap<>();
        sumMiles = new HashMap<>();
        routesSaved = new ArrayList<>();
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public void addNode(City city) {
        if (searchNodeByCityId(city.getCityId()) == null) {
            nodes.add(new Node(city));
        } else {
            System.out.println("  ¡La ciudad existe en el sistema (ಠ︵ಠ)!");
        }
    }

    public void addRouteToNode(int cityId, Route route) {
        try {
            Node node = searchNodeByCityId(cityId);
            if (node != null) node.addRoute(route);
        } catch (Exception e) {
            System.out.println(" ¡El código de la ciudad no se encuentra asociado (ಠ︵ಠ)!");
        }
    }

    public Node searchNodeByCityId(int cityId) {
        for (Node node : nodes) {
            if (node.getCity().getCityId() == cityId) {
                return node;
            }
        }
        return null;
    }

    public Route searchRouteById(int routeId) {
        for (Node node : nodes) {
            for (Route route : node.getRoutes()) {
                if (route.getRouteId() == routeId) {
                    return route;
                }
            }
        }
        return null;
    }

    public List<City> getAllCities(){
        List<City> cities = new ArrayList<>();
        for (Node node: nodes) {
            cities.add(node.getCity());
        }
        return cities;
    }

    public List<Route> findBestRoute(Node origin, Node destination) {
        searchNode(origin.getRoutes(), origin, destination);
        List<Route> routeList = new ArrayList<>();
        for (Integer routeId : sumMiles.get(getBestRoute())) {
            routeList.add(searchRouteById(routeId));
        }

        return routeList;
    }

    public void searchNode(List<Route> routes, Node origin, Node destination) {
        for (Route route : routes) {

            putSeparators(route, origin, routes);
            sum += route.getMiles();
            routesSaved.add(route.getRouteId());

            if (route.getDestination().getCity().getCityName().equals(destination.getCity().getCityName())) {
                sumMiles.put(sum, new ArrayList<>(routesSaved));
                resetCounters();
                return;
            }
            searchNode(route.getDestination().getRoutes(), origin, destination);
        }
    }

    public void putSeparators(Route route, Node origin, List<Route> routes) {
        if (sum == 0 && route.getOrigin() != origin) {
            sum = separator.get(route.getOrigin());
            routesSaved = separatorRoutes.get(route.getOrigin());
        }
        if (routes.size() > 1 && !separator.containsValue(sum)) {
            separator.put(route.getOrigin(), sum);
            separatorRoutes.put(route.getOrigin(), new ArrayList<>(routesSaved));
        }
    }

    public void resetCounters() {
        sum = 0;
        routesSaved.clear();
    }

    public int getBestRoute() {
        return sumMiles.keySet().stream().min(Comparator.comparingInt(a -> a)).orElse(0);
    }

    @Override
    public String toString() {
        return "RouteSystem{" +
                "nodes=" + nodes +
                '}';
    }
}
