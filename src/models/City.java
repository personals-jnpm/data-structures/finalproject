package models;

public class City {

    private int cityId;
    private String cityName;
    private Country country;
    private Weather weather;
    private double height;

    public City() {
    }

    public City(int cityId, String cityName, Country country, Weather weather, double height) {
        this.cityId = cityId;
        this.cityName = cityName;
        this.country = country;
        this.weather = weather;
        this.height = height;
    }

    public City(String cityName, Country country, Weather weather, double height) {
        this.cityName = cityName;
        this.country = country;
        this.weather = weather;
        this.height = height;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return " ciudad: " + cityName + ", " +
                " país: " + country.getCountryName() +
                ", clima: " + weather +
                ", altura: " + height + " "
                ;
    }
}
