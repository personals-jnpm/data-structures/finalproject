package models;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private City city;
    private final List<Route> routes;

    public Node() {
        routes = new ArrayList<>();
    }

    public Node(City city) {
        this.city = city;
        routes = new ArrayList<>();
    }

    public City getCity() {
        return city;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void addRoute(Route route){
        routes.add(route);
    }

    @Override
    public String toString() {
        return  "{ "+ city +
                ", routes: " + routes +
                '}';
    }
}
